from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import search

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class StoryEightTest(TestCase):
    def test_apakah_ada_url_landing_page(self):
        response= Client().get('/search/')
        self.assertEqual(response.status_code, 200)

    def test_jika_tidak_ada_url_landing_page(self):
        response= Client().get('/no_url')
        self.assertEqual(response.status_code, 404)

    def test_apakah_ada_template_landing_page(self):
        response = Client().get('/search/')
        self.assertTemplateUsed(response, 'search.html')

    def test_fungsi_landing_page(self):
        found= resolve('/search/')
        self.assertEqual(found.func, search)

    def test_apakah_ada_search_box(self):
        c = Client()
        response = c.get('/search/')
        content = response.content.decode('utf8')
        self.assertIn('id="search"', content)
    
    def test_apakah_ada_tabel(self):
        c = Client()
        response = c.get('/search/')
        content = response.content.decode('utf8')
        self.assertIn('<table', content)
    
    def test_apakah_menggunakan_staticfiles_benar(self):
        response = self.client.get('/search/')
        self.assertContains(response, 'static/js/search.js')

class StoryEightFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)
        super(StoryEightFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(StoryEightFunctionalTest, self).tearDown()

    def test_cari_buku(self):
        self.browser.get(self.live_server_url+ '/search/')
        time.sleep(3)

        search = self.browser.find_element_by_id('search')
        search.send_keys('HTML')
        search.send_keys(Keys.RETURN)
        time.sleep(3)

        output = self.browser.find_element_by_css_selector('table')
        getOutput = self.browser.execute_script('return arguments[0].innerText', output)
        self.assertIn('HTML', getOutput)

