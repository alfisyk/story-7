from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import index

from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class LandingPageTest(TestCase):
    def test_apakah_ada_url_landing_page(self):
        response= Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_tidak_ada_url_landing_page(self):
        response= Client().get('/no_url')
        self.assertEqual(response.status_code, 404)

    def test_apakah_ada_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_fungsi_landing_page(self):
        found= resolve('/')
        self.assertEqual(found.func, index)
    
    def test_ada_nama(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Muhammad Alfi Syakir', content)

    def test_ada_foto_profil(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('<img src="../static/img/profil.jpg" >', content)

    def test_ada_accordion(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="accordion"', content)

    def test_ada_aktivitas(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Aktivitas', content)

    def test_ada_organisasi_kepanitiaan(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Pengalaman Organisasi/ Kepanitiaan', content)

    def test_ada_prestasi(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Prestasi', content)

    def test_ada_slider_switch_dark_mode(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('switch', content)
        self.assertIn('slider', content)
        self.assertIn('Dark Mode', content)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

    def test_ganti_tema(self):
        self.browser.get(self.live_server_url)
        time.sleep(1)
        dark_theme_button = self.browser.find_element_by_id('pencet')
        dark_theme_button.click()

        time.sleep(1)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(52, 58, 64, 1)')

        time.sleep(1)
        light_theme_button = self.browser.find_element_by_id('pencet')
        light_theme_button.click()

        time.sleep(1)
        body = self.browser.find_element_by_css_selector('body')
        background_color = body.value_of_css_property('background-color')
        self.assertEqual(background_color, 'rgba(255, 255, 255, 1)')

