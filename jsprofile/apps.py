from django.apps import AppConfig


class JsprofileConfig(AppConfig):
    name = 'jsprofile'
