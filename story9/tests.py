from django.test import TestCase, Client
from django.urls import resolve
from . import views
from django.contrib.auth.models import User


from selenium import webdriver
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.
class StoryNineTest(TestCase):
    def test_url_login(self):
        c = Client()
        response = c.get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        c = Client()
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()
        data = {'username':'test', 'password':'test'}

        response = c.post('/story9/login/', data=data)

        response2 = c.get('/story9/login/')
        self.assertEqual(response2.status_code, 302)

        response3 = c.get('/story9/')
        self.assertEqual(response3.status_code, 200)

    def test_not_logged_in(self):
        c = Client()
        response = c.get('/story9/')
        self.assertEqual(response.status_code, 302)

    def test_url_logout(self):
        c = Client()
        response = c.get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

class StoryNineFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome(chrome_options = chrome_options)
        super(StoryNineFunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(StoryNineFunctionalTest, self).tearDown()

    def test_login_function(self):
        user = User.objects.create(username='try')
        user.set_password('trypw')
        user.save()
        
        self.browser.get(self.live_server_url + "/story9/")
        time.sleep(1)
        
        username = self.browser.find_element_by_id('id_username')
        username.send_keys("try")

        password = self.browser.find_element_by_id('id_password')
        password.send_keys("trypw")
        button = self.browser.find_element_by_class_name('button')
        button.click()

        time.sleep(1)



