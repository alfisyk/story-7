from django.shortcuts import render, redirect

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required(login_url='story9:login')
def home(request):
    user = request.user
    return render(request, 'story9.html', {'user':user})

def func_login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('story9:home')
    else :
        if request.user.is_authenticated:
            return redirect('story9:home')
        else:
            form = AuthenticationForm()
    return render(request, 'login.html', {'form':form})

def func_logout(request):
    logout(request)
    return redirect('story9:login')
