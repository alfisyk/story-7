

$(document).ready(function(){
    SearchBook("django");
    $("#search").keypress(() => {
        var code = (event.keyCode ? event.keyCode : event.which);
        if(code == '13'){    
            let key = $("#search").val();
            SearchBook(key);
        }
    })
});

function SearchBook(key){
    $.ajax({
        method: 'GET',
        url: 'https://www.googleapis.com/books/v1/volumes?q=' + key,
        success: function(response){
            $("#hasil").empty();
            document.getElementById('hasil').innerHTML =
            `
            <thead class="thead-dark">
                <tr>
                    <th>No</th>
                    <th scope="col" class="mx-auto text-center">Sampul</th>
                    <th scope="col" class="text-center">Judul</th>
                    <th scope="col" class="text-center">Penulis</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            `;
    
            var contents = response.items
            for(var i = 0; i < contents.length; i++){
                var row = document.createElement("tr");

                var numRow = document.createElement("th")
                numRow.innerHTML = i + 1;
                $(row).append(numRow);

                var thumbnail = document.createElement("td");
                var img = document.createElement("img");
                img.src = contents[i].volumeInfo.imageLinks.thumbnail;
                img.alt = "NotFound"
                $(thumbnail).append(img);
                $(row).append(thumbnail);

                var name = document.createElement("td");
                $(name).append(contents[i].volumeInfo.title);
                name.setAttribute('class', 'text-center');
                $(row).append(name);

                var author = document.createElement("td");
                $(author).append(contents[i].volumeInfo.authors);
                author.setAttribute('class', 'text-center');
                $(row).append(author);
                $("#hasil").append(row);
            }
        }
    })
}